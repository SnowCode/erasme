---
title: "Reflexions Et Reactions"
date: 2021-07-25T20:39:31+02:00
draft: true
---
Erasme le **chrétien**, prêtre et moine, produit des textes de réflexion et de piété, tels que
* *Le Mépris du monde*
* *Le Manuel du soldat chrétien*
* *L’Institution du mariage chrétien*
 
Les ouvrages de piété sont alors les best-sellers absolus d’une société pétrie de christianisme jusque dans les plus petits détails de la vie quotidienne.

Erasme le **courtisan** est peu connu : c’est pourtant lui qui rédige une oeuvre de commande telle que le *Panégyrique de Philippe le Beau*. Evidemment, ce n’est pas son genre de rédaction préféré, mais il faut bien vivre.

Enfin, Erasme le **polémiste** échange des feuilles plus ou moins polies avec ses détracteurs, notamment Luther et ses partisans:
* *Le Libre Arbitre*
* *L’Eponge contre les éclaboussures de Hutten*
* *Le Super Bouclier*
