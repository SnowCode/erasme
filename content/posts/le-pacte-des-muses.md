---
title: "Le Pacte Des Muses"
date: 2021-07-25T20:25:54+02:00
draft: true
---
C’est ainsi qu’Erasme qualifie l’amitié qui l’unit à ses confrères : des savants portés, comme lui, par des valeurs de **développement** et de valorisation de l’Homme.
 
C’est un cercle restreint, mais qui déploie son **réseau** dans toute l’Europe. Fascinés par l’**Antiquité**, à la recherche d’idées neuves, ils sont prêtres, moines, avocats, médecins, théologiens, poètes, fonctionnaires… : ils sont humanistes.
 
Parmi les nombreux amis d’Erasme, on compte ainsi l’Anglais Thomas More, le Français Guillaume Budé, l’Italien Alde Manuce, l’Allemand Reuchlin, l’Espagnol Jean Louis Vives…
 
Erasme les rencontre au cours de ses innombrables **voyages** : à Paris, Bruxelles, Louvain, Anderlecht, Londres, Oxford, Cambridge, Rome, Venise, Bologne, Turin, Bâle, Fribourg… Ses **lettres**, rédigées en latin ou en grec, circulent de l’Espagne à la Pologne, de l’Angleterre à Prague.
 
Parfois même, la rencontre est seulement épistolaire : Erasme n’a jamais vu certains de ses correspondants, tels que Luther ou **Rabelais**.
 
On trouve là l’embryon d’une **Europe unie**, pacifiée, parlant une même langue et partageant un même idéal. Erasme espérait, par la généralisation de l’instruction, l’étendre à tous… et toutes.

**Rêve éphémère** : en fin de vie, Erasme assistera, le coeur brisé, à la division irrémédiable et sanglante de l’Europe entre catholiques et protestants.
