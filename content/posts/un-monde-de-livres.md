---
title: "Un Monde De Livres"
date: 2021-07-25T20:28:23+02:00
draft: true
---
Trois décennies avant Erasme, Gutenberg et son **imprimerie** à caractères mobiles lancent une toute nouvelle industrie, celle du livre. De rare et précieux, celui-ci devient, en quelques années, abondant et **abordable**.
 
La lecture se répand largement dans la société. Ce bouleversement se mesure en une génération : le père d’Erasme était **copiste** manuel ; les oeuvres de son fils ont connu, du vivant de leur auteur, une diffusion à plus d’**un million d’exemplaires**, officiels ou pirates. (Cfr Sources : CROUSAZ)
 
Au travers des échanges épistolaires d’Erasme, on assiste à la naissance de nouvelles **professions** : imprimeurs, éditeurs, libraires. Tout ce monde vit et programme son agenda en fonction de la **Foire Internationale** du Livre de Francfort, véritable carrefour européen, et qui existe encore de nos jours.
 
Les **droits d’auteurs** n’existent pas encore : même un auteur célèbre comme Erasme doit se battre pour toucher le prix de son travail.
 
Avec l’imprimerie naissent les **illustration** en noir et blanc, en commençant par les gravures sur bois.
 
Ce monde du livre, mis en place il y a 500 ans, se retrouve ébranlé de nos jours, dans chacune de ses composantes. Notre génération numérique n’est-elle pas le parfait miroir de celle d’Erasme ?
