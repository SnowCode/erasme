---
title: "Chetif « Corpuscule »"
date: 2021-07-25T20:16:14+02:00
draft: true
---
Longue est la liste des **affections** dont se plaignait Erasme ! Aussi longue est celle de ses **préférences** et **intolérances** alimentaires. Le poisson lui donne mal au ventre, le carême est son calvaire ; la chaleur des grands poêles lui donne la nausée et il hait pour cela les auberges allemandes. Son sommeil est léger : forcé de se réveiller pour les offices nocturnes, au couvent, il n’arrive pas à se rendormir.
 
Il aime **bien manger** ! Quand il étudie à la Sorbonne, de petits billets échangés entre cancres et dont, c’est un comble, la postérité a hérité, nous renseignent sur ses envies de volaille et de lapins rôtis. Ses propres leçons de latin, tout comme ses débats intellectuels, se passent de préférence devant une table bien garnie. Là-dessus, avec le temps, il souffre de la **goutte**, qu’il soigne en buvant du vin de Bourgogne – le seul qu’il trouve acceptable. Il va sans dire que la bière est immonde à son palais.
 
Ses lettres alternent sans transition les considérations professionnelles, les réflexions d’une réelle profondeur philosophique et les jérémiades d’un incorrigible **hypocondriaque**. A mesure que le temps passe, c’est la gravelle, la « **pierre** », qui semble l’affecter le plus, au point de clouer au lit ce bourreau de travail.

Quand on considère, avec le recul, la somme impressionnante de son œuvre, on ne peut qu’admirer l’endurance de ce qu’Erasme appelait lui-même son « corpuscule ».
 
Les sentences gastronomiques d’Erasme émaillent donc le spectacle. Son côté **gringalet souffreteux** a motivé la voie fluette que je lui ai attribuée.
