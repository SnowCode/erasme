---
title: "Sources"
date: 2021-07-25T17:51:15+02:00
draft: true
---

### Œuvres d’Erasme

Parmi les très nombreuses œuvres d’Erasme, voici celles qui ont directement inspiré mon travail :

* *La correspondance d’Erasme*, traduite et annotée d’après le texte latin de l’*Opus epistolarum* de P.S. Allen, H.M. Allen et H.W. Garrod, éd. Aloïs Gerlo (VUB) et Paul Foriers (ULB), Institut pour l’Etude de la Renaissance et de l’Humanisme, s.l.,1967-1984 (12 volumes). 
    * *En particulier les 4 premiers volumes, depuis la prime jeunesse d’Erasme jusqu’à l’irruption de la question luthérienne dans sa vie.*

* Les Funérailles, in Œuvres d’Erasme IV. Le troisième livre des Colloques, traduit du latin par Jarl-Priel, éd. Contantin Castéra, Paris, 1935.
    * *A mes yeux, tous les colloques méritent d’être lus, dont bon nombre sont délicieusement impertinents et irrévérencieux. C’est celui des « Funérailles » qui est en grande partie raconté pendant le spectacle.*

* Erasme : Eloge de la Folie. Adages. Colloques. Réflexions sur l’art, l’éducation, la religion, la guerre, la philosophie. Correspondance, éd. Claude Blum, André Godin, J.-C. Margolin et Daniel Ménager, Robert Laffont, Paris, 1992.
    * *Excellent choix d’œuvres d’Erasme, accompagné d’une chronologie détaillée et d’un dictionnaire. L’Eloge de la Folie est explicitement cité pendant le spectacle.*

* Erasme. Eloge de la Folie et autres écrits, éd. J .-C. Margolin, Folio Classique, Saint-Amand, 2010.
    * *Autre bonne anthologie, conçue comme complémentaire à la précédente.*

### Ouvrages sur Erasme

* BIERLAIRE Franz, La familia d’Erasme. Contribution à l’histoire de l’humanisme, Librairie philosophique J. Vrin, Paris, 1968.
    * *Un saisissant portrait d’Erasme au quotidien, sa maisonnée, son train de vie et ses élèves.*

* CROUSAZ Karine, Erasme et le pouvoir de l’imprimerie, Ed. Antipodes, Lausanne, 2005.
    * *Passionnante étude sur l’émergence du livre imprimé et les débuts de cette industrie. Ce livre a fourni les informations nécessaires à un « communiqué radio » du spectacle portant sur la Foire Internationale de Francfort.*

* DELCOURT Marie, Erasme, Bruxelles, 1986.
* HALKIN Léon, Erasme parmi nous, Fayard, s.l., 1987
    * *L’ouvrage de référence à partir duquel s’est charpentée ma narration.*

* RICHARDT Aimé, Erasme. Une intelligence au service de la paix, éd. François-Xavier de Guibert, Paris, 2010.
    * *N’apporte rien de bien neuf, mais cite largement le pamphlet, anonyme mais furieusement erasmien, du « Julius Exclusus » (le pape Jules II exclu du paradis). Un régal.*

* VAN DAMME Daniel, Erasme. Sa vie, ses œuvres, Bruxelles, s.d.
    * *Un peu vieilli actuellement, mais intéressant pour sa riche iconographie.*

* VANAUTGAERDEN Alexandre, Autoportraits d’Erasme, éd. Maison d’Erasme, Brepols, s.l. 2010.
* ZWEIG Stefan, Erasme. Grandeur et décadence d’une idée, Le Livre de Poche, Malesherbes, 2010.

    * *Magnifique essai, sensible et personnel, sur le caractère d’Erasme, ses idéaux et ses illusions perdues. La meilleure porte d’entrée, pour le nouveau venu, vers le monde d’Erasme.*

### Autour d’Erasme 

Pour comprendre le siècle d’Erasme, il faut lire largement sur ses (presque) contemporains, ceux qu’il a fréquentés (Luther, François Ier, Charles Quint, Henri VIII, Alde Manuce, Thomas More) ou non (Lorenzo Valla, Christophe Colomb). Tous ceux cités ici le sont également dans le spectacle, et en particulier :

* MORE Thomas, L’Utopie, ou le Traité de la meilleure forme de gouvernement, GF-Flammarion, Paris, 1987.

Presque tout le dialogue final entre Erasme et Luther est constitué de citations authentiques, néanmoins condensées et adaptées à un style oral. Les assertions de Luther ont été puisées sur :

* LIENHARD Marc, Luther et la liberté chrétienne. Le salut et la liberté, in La Revue Réformée (Revue de théologie de la faculté Jean Calvin), n°244 (en ligne), s.l., s.d. (consulté en décembre 2013)       http://larevuereformee.net/articlerr/n244/luther-et-la-liberte-chretienne-le-salut-et-la-liberte
* LUTHER Martin, 95 thèses, sur Wikipedia (en ligne), sl., s.d. (consulté en décembre 2013). Voir en particulier les thèses 21, 27 et 86.
* LUTHER Martin, Déclaration à la Diète de Worms, sur Wikipedia (en ligne), sl., s.d. (consulté en décembre 2013).

Pour témoigner de la formation du jeune Erasme est lu un extrait de :

* D’OCKHAM Guillaume, Commentaire du premier livre des sentences, trad. E. Karger, dans POLET Jean-Claude (éd.), Patrimoine littéraire européen, De Boeck-Larcier, Bruxelles, 1995, t.5, p. 298.

Guillaume d’Ockham (né vers 1285, mort en 1359) est bien antérieur à Erasme, mais son texte est cité comme exemple de la scolastique que l’on étudiait en faculté de théologie, encore à la Renaissance. Erasme, élève à Deventer, chez les Frères de la Vie Commune, fut également en contact avec la devotio moderna, mouvement de piété dont le souvenir le plus vivace s’est transmis via

* A KEMPIS Thomas, L’imitation de Jésus-Christ, trad. F. de Lamennais, Brepols S.A., Malines, 1919.

Par certains aspects, cet ouvrage du XVe siècle a représenté un progrès dans la réflexion autour de la pratique chrétienne occidentale (piété personnelle, sincérité, simplicité) et son influence sur Erasme a été largement démontrée. D’autres côtés de l’oeuvre, par contre, s’éloignent radicalement du message érasmien : culpabilisation, valorisation de la soumission et de la souffrance. J’y ai puisé, au chapitre XXIV, une irrésistible description de l’enfer, digne de Jérôme Bosch.

Il n’a pas été nécessaire de remonter les siècles pour retrouver l’idée selon laquelle on devrait se soumettre, corps et esprit, à une autorité religieuse. La citation faite en introduction du spectacle provient de

* WATCHMAN NEE, Leçons pour nouveaux croyants, dans Living Stream Ministry, (en ligne), sl, 2014, (consulté le 1er avril 2014)       http://www.lsm.org/francais/24/Lesson17.html

Pour illustrer l’apprentissage du latin par le jeune Erasme, j’ai eu recours aux premiers vers de l’Enéide, de Virgile, scandé selon l’exemple du Classical Language Instruction Project, de Princeton (en ligne). La règle de Saint-Benoît (chapitre 8) est citée en latin pour signifier au jeune Erasme qu’il doit prier même la nuit. Peu auparavant, c’est le Veni Creator, hymne grégorien bien connu, qui est massacré par le moinillon plein de mauvaise volonté. Enfin, le nulli concedo final du spectacle n’est autre que la devise d’Erasme, qu’il portait à son doigt, sur une bague antique avec une effigie qu’il attribuait au dieu Terminus, la Mort. J’ai choisi l’interprétation qu’en donne Stefan Zweig, « Je n’appartiens à personne », en conclusion du spectacle.
