---
title: "Autour d'Erasme"
date: 2021-07-25T17:18:04+02:00
draft: true
---
### Le héros
* Nom inconnu.
* Pseudonyme : Desiderius Erasmus Roterodamus
* Rotterdam, 1467 ?  –  Bâle, 1536.
* [Fils de prêtre](/posts/parents-tuteurs)
* Moine, théologien, philosophe, philologue, humaniste
* [Hypocondriaque](/posts/chetif-corpuscule), [ambitieux](/posts/quel-caractere), rusé, bosseur, gourmet, désargenté
* Ecrit, parle, vit et pense en latin
* Valeurs : liberté, raison, paix, amitié

### Le contexte
L’[Europe il y a 500 ans](/posts/la-renaissance) : morcelée. Les états se font la guerre. La peste circule. Tout le monde se dit chrétien, sous l’autorité du pape. Grâce à Christophe Colomb, on confirme que la terre est ronde et que l’Europe n’en est pas le centre. Dans les Balkans, les Turcs avancent. Ils arrivent en Hongrie.
 
### L'internationale humaniste
Toute l’[Europe savante](/posts/le-pacte-des-muses) parle et écrit en latin. 
On recherche les manuscrits les plus anciens, on redécouvre les auteurs antiques. 
Pour les comprendre, on étudie les langues anciennes. 
Pour les diffuser, une nouvelle technologie : l’imprimerie. Le [livre](/posts/un-monde-de-livres) se répand. 
Le plus périlleux : étudier les premiers écrits chrétiens. L’Inquisition punit tout écart de pensée. 
Pour les humanistes, cependant, la liberté est essentielle.

### L’oeuvre d’Erasme
L’oeuvre d’Erasme est immense et très variée: [oeuvres satiriques](/posts/pamphlets-et-sarcasmes), dont « L’Eloge de la Folie », [études du latin](/posts/expertise-linguistique), textes [philosophiques et religieux](/posts/reflexions-et-reactions), éditions d’[auteurs antiques](/posts/edition-dauteurs-antiques), édition critique du Nouveau Testament, des milliers de lettres.
Ses livres, écrits en latin, sont moins lus de nos jours, mais son influence sur la pensée européenne est considérable.

### Les têtes couronnées
Erasme entretient des relations épistolaires avec les principales personnalités de son temps, notamment des [rois](/posts/des-rois-remuants) : Charles Quint, roi d’Espagne et empereur germanique ; François 1er, roi de France ; Henry VIII, roi d’Angleterre.

### Les guerres de religions
Au début du 16e siècle, l’Eglise contrôle en grande partie la société. Le pape se conduit souvent comme un seigneur de guerre. Erasme dénonce les dérives de l’Eglise : corruption du clergé, dogmes infantilisants, police de la pensée.
Il appelle à un retour aux valeurs fondatrices du christianisme : pauvreté, simplicité, bienveillance pour tous. Il souhaite, par ses travaux, faire mieux connaître les évangiles.
Un moine allemand s’élève aussi contre les abus de l’Eglise : [Martin Luther](/posts/les-guerres-de-religion). Cependant, sa véhémence, son extrémisme rebutent Erasme, qui souhaite une réforme en douceur.
L’heure est au radicalisme : Erasme, pris entre luthériens et papistes, sera rejeté par les deux camps.
Les guerres de religion se multiplient. L’Europe sera désormais divisée entre catholiques et protestants.

### Débats
Aujourd’hui encore, Erasme pose question. Par exemple :
Etait-il [catholique, protestant ou libre penseur](/posts/catholique-protestant-ou-libre-penseur) ?
Sa [neutralité](/posts/intrepide-pamphletaire-ou-couard-magnifique) face au conflit : courage ou couardise ?
Et la [réforme de l’Eglise](/posts/la-modernisation-de-leglise-catholique) catholique, finalement ?
L’Homme est-il aussi [libre](/posts/le-libre-arbitre) qu’Erasme le disait ?
