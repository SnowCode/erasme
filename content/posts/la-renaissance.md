---
title: "La Renaissance"
date: 2021-07-25T20:21:00+02:00
draft: true
---
La vie d’Erasme illustre et incarne la Renaissance, période fascinante marquée de **changements profonds**.
Pour la génération d’Erasme, comme pour la nôtre, le temps s’est accéléré, l’univers s’est agrandi, les connaissances se sont emballées.

### Un monde élargi 
* Christophe Colomb par l’Ouest, Vasco de Gama par l’Est : en quelques années, les Européens prennent conscience d’un **monde infiniment plus vaste** et plus diversifié que ce qu’ils imaginaient.
* Il y a peu d’échos chez Erasme de ce nouveau monde : lorsqu’il parle de l’univers entier, il vaut mieux comprendre l’Europe, avec, éventuellement, ses voisins turcs.
* Néanmoins, la découverte de ***terrae incognitae*** inspire son époque : en témoigne l’**Utopie**, de Thomas More, ailleurs fantasmé, île fabuleuse dotée du meilleur gouvernement qui soit.
 
### Une carte d’Europe peu reconnaissable
* Dans le continent européen sillonné par Erasme, la plupart des **pays** que nous connaissons n’existent pas encore. La France, l’Espagne, le Portugal et la Suisse se dessinent à peine ; l’Angleterre a des démêlés avec l’Ecosse ; l’Italie et l’Allemagne sont morcelées ; la Pologne est immense.
* Certes, les **nationalismes** s’expriment déjà : à travers la correspondance des humanistes, les Français et les Anglais se brocardent mutuellement, personne n’aime la cuisine allemande et les Italiens traitent le reste du monde de barbare.
* Entre la France et l’Allemagne s’étend une région sous domination espagnole, nommée cependant « **Bourgogne** » ou « **Pays-Bas** » et où l’on parle français et néerlandais. Erasme lui-même, natif de l’endroit, a du mal à y voir clair :  *Suis-je un Batave ? Je n’en suis pas trop sûr ; mais je ne puis nier que je sois Hollandais. Je suis né dans une région qui, si nous en croyons les cartes des cosmographes, se tourne plutôt vers la France que vers l’Allemagne, bien qu’il soit hors de doute que cette région soit tout entière limitrophe et de la France et de l’Allemagne.* (Lettre 1147, A Pierre Manius, Louvain, 1er octobre 1520)
