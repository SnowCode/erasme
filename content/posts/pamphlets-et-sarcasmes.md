---
title: "Pamphlets Et Sarcasmes"
date: 2021-07-25T20:31:35+02:00
draft: true
---
C’est surtout par ses oeuvres satiriques qu’Erasme est resté dans les mémoires.
 
L’***Eloge de la Folie***, chef-d’oeuvre absolu, donne la parole à Dame *Folie*, **bienfaitrice auto-proclamée de l’humanité**, pour une plaidoirie évidemment insensée et toute paradoxale.
 
*En somme, si on regardait de la lune les agitations innombrables des mortels, comme le fit jadis Ménippe, on croirait voir une nuée de mouches ou de moucherons qui se battent, se font la guerre, se tendent des pièges, pillent, jouent, folâtrent, naissent, tombent et meurent. On ne saurait croire quels troubles, quelles tragédies provoque un si minuscule animal et destiné à périr si tôt. Car quelquefois la moindre bourrasque d’une guerre ou d’une épidémie en emporte et en détruit des milliers en même temps.*
 
#### (Eloge de la Folie, XLVIII, éd. J.-C. Margolin, p. 107) {.right}
 
Citons aussi le ***Jules exclu du ciel***, pamphlet non signé mais si érasmien que son anonymat est déjà percé du vivant de son auteur.
 
Il faut le lire pour le croire : Erasme imagine Jules II, pape guerrier et corrompu, récemment décédé, se trouvant bloqué à l’entrée du paradis et sommant Saint Pierre, avec de terribles imprécations, de lui ouvrir la porte !

* JULES – *En voilà assez, je suis Jules le Ligurien, P.M. …*
* PIERRE – *P.M. ! Qu’est-ce ? Pestis Maxima ?*
* JULES – *Pontifex Maximus, coquin !*
* PIERRE – *Même si tu es trois fois Maximus, tu ne peux pas entrer sans être aussi Optimus.*
* JULES – *Impertinence ! Toi qui n’as été, au cours des âges, jamais plus que Sanctus, alors que je suis Sanctissimus, Sanctissimus Dominus, Sanctitas, Sainteté, avec des bulles pour le prouver.*
* PIERRE – *N’y a-t-il pas une différence entre être saint et être appelé Saint ? Laisse-moi regarder de plus près. Hum ! De nombreux signes d’impiété… Une soutane de prêtre, mais recouverte d’une cuirasse ensanglantée ; des yeux sauvages, une bouche insolente, un front effronté, un corps recouvert des stigmates de tes vices, une haleine empuantie par le vin, une santé mise à mal par les débauches. (…)*
* JULES – *Arrête-toi, ou je t’excommunie…*
* PIERRE – *M’excommunier, moi ? Et de quel droit, je te prie ?*
* JULES – *Le meilleur des droits, tu n’es qu’un prêtre, et peut-être même pas…*

 
#### (Jules exclu du ciel, cité par RICHARDT, p. 68-69) {.right}

On retrouve souvent la même ironie dans les autres oeuvres d’Erasme – il est vrai, à dose moins concentrée…
