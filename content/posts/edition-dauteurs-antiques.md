---
title: "Edition Dauteurs Antiques"
date: 2021-07-25T20:42:21+02:00
draft: true
---
Il s’agit d’**éditions critiques** : pour chaque oeuvre, Erasme

* rassemble les manuscrits les plus anciens qu’il puisse trouver
* les compare pour en restaurer la meilleure version possible
* imprime cette version, souvent assortie de commentaires

en espérant que le livre ainsi produit se vendra bien, et qu’il en retirera quelque bénéfice.

Grand **chasseur de manuscrits**, lecteur infatigable, éditeur à l’**activité prodigieuse**, Erasme laisse loin derrière lui tous ses collègues philologues en terme de quantité et, pour l’époque, de qualité.
 
Erasme s’est ainsi penché sur

* des auteurs **païens** :  Cicéron, Sénèque, Suétone, Quinte Curce, Quintilien, Térence, Lucien, Euripide, Démosthène, Plutarque, Horace, Ovide, Tite-Live, Plaute, etc.
* des auteurs **chrétiens** tels que Jérôme, Cyprien, Hilaire, Jean Chrysostome, Ambroise, Athanase, Arnobe, Augustin, Basile, Origène, etc.

Il a également traduit en latin de certaines oeuvres grecques de Lucien, Euripide, Plutarque, Origène
 
### Un cas épineux
L’édition critique et la nouvelle traduction latine du **Nouveau Testament** (sauf l’Apocalypse) est à considérer à part.

Inspiré par les travaux de son prédécesseur, Lorenzo Valla, Erasme entreprend là un travail aussi salutaire que dangereux. **Salutaire**, parce qu’un texte qui régit à ce point les vies et les consciences doit être clair, exact et bien compris de tous. **Dangereux**, parce que les censeurs sont nombreux, puissants et sourcilleux. Toucher aux « saintes écritures », les traiter comme un texte antique susceptible d’avoir été altéré par les transcriptions successives des copistes, c’est introduire le doute dans la foi, c’est soumettre le tabou au crible de la raison critique.

Le Nouveau Testament d’Erasme servit de base de travail à Luther et à tous ces pionniers qui osèrent traduire la « parole divine » en **langues modernes**. Dès lors, si Dieu parle ma langue, quel besoin ai-je des intermédiaires et maîtres à penser ? C’était le germe, largement diffusé, de la Réforme protestante et ensuite de la raison critique.

### Développement de la philologie ; création du Collège des Trois Langues 
Grâce au travail considérable d’Erasme et de ses confères humanistes, des oeuvres anciennes furent exhumées des bibliothèques médiévales, déchiffrées, corrigées, publiées et largement diffusées.
 
Leur attention à la correction de la langue a permis de rectifier des erreurs qui s’étaient glissées lors des multiples transcriptions manuscrites, mais aussi de démasquer parfois des faux. Ils sont les créateurs de la **philologie**, qui étudie les langues sur base des textes écrits, à l’aide de la critique littéraire, de l’histoire et, actuellement, de la linguistique.

Pour promouvoir cette méthode, pour étudier et diffuser les « bonnes lettres », Erasme contribue à mettre sur pied, en 1517, le **Collège des Trois Langues**, adjoint à l’Université Catholique de Louvain. Il n’y enseigne pas lui-même, mais son large réseau lui permet de trier et recruter la fine fleur de l’humanisme pour y donner des cours de latin, de grec et d’hébreu.
 
En butte aux tracasseries des théologiens louvanistes et devant la montée des tensions religieuses, Erasme finit par quitter la ville et se réfugie à Bâle, en Suisse. Son Collège survivra jusqu’à la fin du XVIIIe siècle.
 
La philologie « classique », quant à elle, est encore enseignée de nos jours et se concentre désormais sur les seules langues latine et grecque.
