---
title: "Citations"
date: 2021-07-25T16:35:30+02:00
draft: true
---

Petite cueillette piquante et purement subjective…

*Sauf mention contraire, les extraits de lettres suivent la traduction proposée par l’édition d’Aloïs Gerlo et Paul Foriers. (cfr sources)`*
 
## AMITIE, le sel de la vie
*Je vois la plupart des mortels partager la conviction qu’ils doivent rechercher les causes de leur bonheur ou de leur malheur dans les corps célestes. Je ne pense pas qu’aucune étoile brille pour personne avec plus de bonheur qu’un ami sincère et accordé à son esprit (…). Que les autres observent les astres s’il leur plaît ; moi, j’estime qu’il faut chercher sur la terre ce qui nous rend heureux ou malheureux.*

#### (Lettre 1005, 10 août 1519, à Pierre Zutphen de Cassel) {.right}


*Le lien entre nous, parti de principes bien meilleurs, repose sur des fondements autrement solides, puisque ce ne sont ni l’intérêt, ni la recherche du plaisir, ni une passion puérile qui nous ont rapprochés, mais le noble amour des lettres et de nos communes études.*

#### (Lettre 63, à Thomas Grey, Paris, août 1497) {.right}

 
## ANDERLECHT, ressourcement campagnard
*Je t‘écris ces lignes de notre campagne d’Anderlecht, où, poussé par ton exemple, je me suis mis, moi aussi, à vivre aux champs. (…) Cette vie rustique me fait tellement de bien que je suis désormais disposé à la répéter chaque année.*

#### (Lettre 1233, à Guillaume Budé, Anderlecht, septembre 1521) {.right}

 
## ANGLETERRE, pays de rêve
*Ton Angleterre a bien des raisons de me plaire infiniment, et celle-ci en premier lieu qu’elle abonde en ce qui me semble le plus précieux au monde, en hommes très versés dans les bonnes lettres.*
#### (Lettre 107, à John Colet, Oxford, octobre 1499) {.right}

*… jamais rien jusqu’à présent ne m’a autant plu. J’ai trouvé ici le climat le plus agréable et le plus salubre ; une telle humanité, une telle érudition, non point banale et commune, mais distinguée, précise, antique, latine et grecque, à tel point que sans mon envie de voir l’Italie, je n’aurais pas grand-chose à regretter.*
#### (Lettre 118, à Robert Fisher, Londres, 5 décembre 1499) {.right}

 
## ARGENT, pénurie permanente justifiant toutes les impudences
*Ô mendicité ! (…) Je me déteste moi-même et j’ai décidé ou de découvrir quelque chance qui me libère de toujours quémander, ou d’imiter en tout Diogène.*
#### (Lettre 227, à John Colet, Cambridge, 13 septembre 1511) {.right}

*Tu es peut-être d’avis, toi, que mes affaires vont bien du moment que je ne suis pas acculé à la mendicité. Pour moi, voici à quel point je suis réduit : il me faut soit abandonner complètement mes études, soit trouver n’importe où les ressources qu’exige le métier d’écrivain. Celui-ci suppose une vie qui ne soit pas absolument misérable et humiliée. N’étais-je pas tout pareil à un mendiant ? Ne le suis-je pas encore puisque je n’ai pas un sous tournois en poche ? J’ai honte d’en dire plus long. Regarde autour de toi : que d’ânes illettrés couverts de richesses ! Et c’est déjà bien beau, selon toi, qu’Erasme ne meure pas de faim ?*
#### (Lettre 139, à Jacques Batt, Orléans, vers le 12 décembre 1500) {.right}

*… j’entends un pécule digne de moi. Il m’est en effet impossible de venir à mes frais, vu mon dénuement (…). Je voudrais aussi que tu me fisses envoyer, si possible, un meilleur cheval. Je ne demande pas un magnifique Bucéphale, mais une bête qu’on n’ait pas honte de montrer. Et tu sais que j’ai besoin de deux chevaux, car j’ai décidé d’amener de toute façon mon élève avec moi.*
#### (Lettre 80, à Jacques Batt, pour demander une aide financière et matérielle, Paris, 29 novembre 1498) {.right}

 
## CARACTERE : autoportrait pour le moins complaisant
*Vois en moi un homme de fortune médiocre, ou, plus exactement, nulle, étranger à toute ambition, très enclin à aimer, quelque peu exercé aux lettres, mais des plus ardents à les admirer, qui révère religieusement l’honneur des autres et qui tient le sien pour rien ; qui se reconnaît volontiers inférieur à tous pour la science, mais à personne pour la loyauté ; simple, ouvert, sincère, également incapable de feindre et de dissimuler ; d’un caractère timide, mais droit : un homme peu bavard ; un homme enfin duquel tu n’as rien à attendre si ce n’est du cœur.*
#### (Lettre 107, à John Colet, Oxford, octobre 1499) {.right}

 
## CHRISTIANISME, universelle amitié 
*Comme si le christianisme était autre chose qu’une vraie et parfaite amitié, autre chose que de mourir en Christ, vivre en Christ, n’être avec Christ qu’un corps et une âme ; ainsi les hommes sont comme étroitement liés entre eux, à l’instar des membres du corps.*
#### (Lettre 187, à Richard Foxe, Londres, 1er janvier 1506) {.right}

 
## COUVENT de Steyn, d’où il convient de s’échapper à tout prix
*Je ne désire qu’une seule chose, qu’on m’accorde le loisir où je puisse vivre tout entier pour Dieu seul, me plonger dans les saintes Ecritures, lire ou écrire un peu. C’est ce que je ne puis faire dans la retraite ou communauté. Rien de plus délicat que moi : ma santé, même quand elle est au mieux, ne supporte ni les jeûnes, ni les veilles, ni aucune incommodité. Ici, où je vis dans un tel confort, voilà que brusquement je tombe malade ; que ferais-je dans les fatigues d’une communauté ?*
#### (Lettre 75, à Arnold de Bosch ; Paris, vers avril 1498) {.right}
*Je suis méprisé chez vous par des hommes fort bêtes et fort ignorants, qui s’imaginent que la piété réside tout entière dans le froc et la sévérité ; il n’y a rien de plus aisé que de mépriser ce que l’on n’a pas ; rien non plus de plus sot.*
#### (Lettre 171, Au père Nicolas Werner, prieur de Steyn, Louvain, septembre 1502) {.right}
*Tu sais en effet que j’ai été poussé plutôt que conduit à cette profession par l’obstination de mes tuteurs (…) ; que j’y fus maintenu par les reproches de Corneille de Woerden et par une sorte de honte enfantine, alors que je n’étais nullement fait pour elle ; car les mêmes choses ne conviennent pas à tous. Je n’ai jamais pu supporter les jeûnes, et cela en vertu d’une disposition particulière de mon corps. Une fois réveillé, je n’ai jamais pu me rendormir, sinon après plusieurs heures. Mon esprit se portait uniquement vers les lettres, dont on n’a que faire chez vous…*
*J’ai toujours considéré comme la pires de toutes (choses) le fait d’avoir été jeté dans ce genre de vie auquel j’étais le plus étranger, et d’esprit et de corps (…). On m’objectera, à vrai dire, mon année de probation (comme on l’appelle) et mon âge d’homme. C’est ridicule. Comme si l’on pouvait demander à un enfant de 17 ans, élevé surtout dans les livres, de se connaître lui-même, ce qui est difficile même pour un vieillard…*
#### (Lettre 296, au révérend père Servais Roger, prieur de Steyn, château de Harn, 8 juillet 1514) {.right}

 
## DIEU est amour
*A la vue de ton frère, traité indignement, tu ne t’émeus pas, pourvu que tu sois sauf : pourquoi ton âme ne réagit-elle pas ? Parce qu’elle est morte. Pourquoi est-elle morte ? Parce qu’il n’y a plus en elle son souffle de vie, Dieu. Car là où est Dieu, là est l’amour.*
#### (Erasme, Manuel du soldat chrétien, cité par HALKIN, p. 97) {.right}
*Plutôt moins savoir et aimer davantage que savoir davantage et ne point aimer.*
#### (Erasme, Manuel du soldat chrétien, cité par HALKIN, p. 92) {.right}
*Ne me dis pas que la charité consiste à fréquenter les églises, à se prosterner devant les statues des saints, à faire brûler des cierges et à compter les prières que tu répètes. Dieu n’a pas besoin de tout cela. (…) Je ne réprouve pas le culte extérieur, mais Dieu n’est satisfait que par le culte intérieur.*
#### (Erasme, Manuel du soldat chrétien, cité par HALKIN, p. 93) {.right}

 
## ENFER : qu’est-ce au juste ?
*La perpétuelle angoisse qui accompagne l’habitude du péché.*
#### (Erasme, Manuel du soldat chrétien, cité par HALKIN, p. 96) {.right}

 
## ESQUIVE : un art bien maîtrisé
*Enfin, je ne suis pas assez stupide pour me compromettre, de mon propre mouvement, dans une affaire aussi odieuse, et à laquelle je devrais asservir ma vie tout entière, alors que je peux la passer en spectateur paisible.*
#### (A propos de l’affaire de Luther. Lettre 1153, à Godescalc Rosemondt, Louvain, 18 octobre 1520) {.right}
*Le roi d‘Angleterre (…) m’a demandé ce que je pensais de toi. J’ai répondu que tu étais un personnage trop savant pour que je puisse, moi, un individu dont l’érudition est insuffisante, me prononcer sur ton compte. *
#### (Lettre 1127A, à Martin Luther, Louvain, 1er août 1520) {.right}

 
## FILLES : Thomas More a raison, instruisons-les également
*Il n’était pour ainsi dire personne sur terre qui ne tînt pour un axiome que, pour le sexe féminin, la littérature était inutile aussi bien à la chasteté qu’à la bonne réputation. Et moi-même, autrefois, je n’étais guère opposé à cette manière de voir ; mais vraiment More me l’a entièrement chassée de l’esprit. (…)*
*Comme le charme et la solidité du mariage procèdent davantage du bon vouloir des esprits que de l’amour charnel, les époux, qu’unit, par-dessus le marché, l’harmonie des dispositions intellectuelles, sont unis par des liens beaucoup plus solides. (…) c’est avec de pareilles femmes qu’il est charmant de vivre ensemble.*
#### (Lettre 1233, à Guillaume Budé, Anderlecht, septembre 1521) {.right}

 
## GRAMMAIRE : science modeste, mais première
*Beaucoup d’hommes sont capables de juger correctement sans avoir étudié la logique. Sans la connaissance du langage, personne ne peut comprendre ce qu’il entend ou ce qu’il lit.*
#### (cité par HALKIN, pages 30-31) {.right}
*La grammaire est d’un rang inférieur à plusieurs autres sciences, mais aucune n’apporte une aide plus indispensable. Elle s’occupe de choses fort petites, mais sans lesquelles personne ne peut devenir très grand ; elle agite des bagatelles, mais qui entraîne des conséquences sérieuses. Prétendre que la théologie est trop grande pour se plier aux lois de la grammaire et que tout le travail d’interprétation dépend de l’inspiration du Saint-Esprit, c’est vraiment donner aux théologiens une dignité toute nouvelle. A eux seuls, il serait permis de parler une langue barbare. (…) S’il a pu se faire que les traducteurs de l’Ancien testament se soient trompés en certains endroits, (…) n’ont-ils pas pu aussi se tromper dans le Nouveau ? (…) Allons-nous faire remonter les erreurs au Saint-Esprit et conclure qu’il en est l’auteur ?*
#### (Lettre 182, à Christophe Fisher, Paris, vers mars 1505) {.right}

 
## GUERRE, indigne des chrétiens, indigne des hommes
*Je me suis souvent interrogé sur les raisons qui poussent, je ne dis pas les chrétiens, mais tous les hommes à ce degré de folie où au prix de tant d’effort, de dépenses, de dangers, ils se lancent à leur mutuelle destruction. Que faisons-nous d’autre, notre vie durant, que de nous combattre ? (Les animaux, eux,) ne vont pas à leur destruction réciproque en troupeaux compacts, comme nous. Nous qui nous glorifions de nous désigner d’après le nom du Christ, qui n’a enseigné que la douceur et qui en a donné l’exemple, (…) une seule chose en ce monde peut-elle être assez importante pour nous exciter à la guerre ? Une chose si néfaste et si sombre qu’alors même qu’elle est des plus justes, nul véritable homme de bien ne saurait l’accepter…*
*Et quel royaume, à ton estimation, vaudrait la vie et le sang de tant de milliers d’hommes ? (…) Que penses-tu qu’éprouvent les Turcs quand ils apprennent que les princes chrétiens s’entre-déchirent en mêlées insensées, et cela simplement pour avoir le droit de gouverner ? L’Italie a été enfin reconquise sur les Français. Le seul résultat de ce carnage c’est que le pays auparavant administré par un Français l’est actuellement par un autre. Et le pays était plus prospère naguère qu’à présent. (…) Je te le demande, trouves-tu humain que l’univers doive prendre les armes chaque fois que tel ou tel prince se met en colère contre un autre (…) ?*
#### (Lettre 288, à Antoine de Berghes, Londres, 14 mars 1514) {.right}

 
## IMPRIMEURS : les imprimeurs paient mal, les droits d’auteurs n’existent pas encore et les éditions pirates abondent…
*Pour la révision des lettres de Jérôme je te donnerai volontiers 15 autres florins et autant pour ce que tu m’as envoyé à présent. Aïe, diras-tu, quel prix minime ! Je ne pourrai, je te l’avoue, te donner la rétribution que méritent ton intelligence, ton travail, ton savoir et tes fatigues ; mais les dieux et ton caractère même te donneront d’abord la plus belle récompense. (…) tu aideras ton petit Bade qui a pour tout profit une nombreuse postérité et le travail de chaque jour. Courage Erasme, notre cher défenseur. Envoie-moi ton accord…*
#### (Lettre 263, de Josse Bade, imprimeur, à Erasme, Paris, 19 mai 1512) {.right}

 
## LANGUES
*… je suis trop éloigné des idiomes hollandais, qui s’entendent à nuire abondamment et ne sont utiles à personne.*
#### (Lettre 171, Au père Nicolas Werner, Louvain, septembre 1502) {.right}
*(le français, langue) barbare et anormale qui s’écrit autrement qu’elle se prononce et qui possède des sons stridents et des accents qui n’ont presque plus rien d’humain.*
#### (Cité par HALKIN, p.172) {.right}
*Mon affaire à moi, ce sont les auteurs grecs et latins.*
#### (Lettre.1499 I, 10-12, citée par BIERLAIRE) {.right}

 
## LETTRES et littératures, porteuses de réflexion, de sens et de liberté
*Fasse le ciel que Luther eût suivi mon conseil, et se fût abstenu de ces propos haineux et séditieux. Celui lui eût apporté plus de profit et moins de haine. Ce serait peu de chose que la perte d’un homme, mais, si ses ennemis réussissent, personne ne pourra plus supporter leur insolence. Ils n’auront pas de repos avant d’avoir détruit les langues et la littérature humanistes. (…) C’est de la haine des belles-lettres et de la stupidité des moins qu’est issue cette tragédie ; (…) Quel est leur but, personne n’en doute : c’est évidemment d’écraser ces littératures qu’ils ignorent et d’imposer impunément leur barbarie. Quant à moi, je ne me mêle pas de cette tragédie. Au demeurant, un épiscopat m’attend, si je consentais à écrire contre Luther.*
*Je m’afflige de ce que la doctrine évangélique soit ainsi étouffée, de ce que nous soyons uniquement contraints et non instruits, et que l’on enseigne ce qui est étranger à la fois aux Saintes Ecritures et au sens commun.*
#### (Lettre 1141, à Gérard Geldenhauer, Louvain, 9 septembre 1520) {.right}

 
## LIBERTE de conscience
*Il est plus facile de vaincre avec des bulles et de la fumée qu’avec des arguments.*
#### (Lettre 1166, à N., à propos des répressions entre catholiques et protestants, Louvain, décembre 1520)
*Il n’est aucun mortel que je juge assez important pour me laisser entraîner à partager toutes ses opinions.*
#### (Lettre 1153, à Godescalc Rosemondt, Louvain, 18 octobre 1520) {.right}
*Il faut d’abord examiner quelles sont les sources de ce mal. Le monde est accablé par les institutions humaines. Il est accablé par les opinions et les dogmes scolastiques. Il est accablé par la tyrannie des frères mendiants (…) Je ne les condamne pas tous, mais la plus grande part sont de telle espèce qu’ils enchaînent, de propos délibéré, la conscience des hommes pour obtenir un gain et exercer leur tyrannie. (…) Ce qui ne plaît pas, ce qu’on ne comprend pas, c’est une hérésie. Savoir le grec est une hérésie. Un langage cultivé est une hérésie. Ce qu’ils ne font pas eux-mêmes est une hérésie. J’avoue que le grief d’une foi faussée est grave, mais il ne faut pas tout ramener à une question de foi.*
#### (Lettre 1033, à Albert de brandebourg, cardinal archevêque, 19 octobre 1519) {.right}

 
## LIVRES
*Je m’occupe de mes amis et jouis de leur agréable intimité. (…) C’est en leur compagnie que je m’enferme dans mon petit coin et, fuyant la foule inconstante, ou bien je leur murmure de doux propos, ou bien je prête l’oreille à ceux qu’ils me chuchotent (…) Ce que je t’ai dit jusqu’ici des amis, entends-le des livres, dont la compagnie me rend pleinement heureux.*
#### (Lettre 125, à N, Paris, Printemps 1500) {.right}

 
## LOUVAIN
*La peste, qui m’a fait partir, me retient toujours à Louvain (…). Tout me plaît à Louvain, si ce n’est que la vie y est bien rustique et trop coûteuse, et que, de plus, je n’y puis rien gagner du tout.*
#### (Lettre 172, à William Hermansz, Louvain, septembre 1502) {.right}
*… on trouve chez les Louvanistes seuls des gens qui résistent aux lettres humanistes avec tant de ténacité. (…) Et pourtant, je ne sais s’il y a une nation où les lettres humanistes se portent mieux qu’ici.*
*… Ce qui m’a fait venir à Louvain, c’est uniquement un climat plus sain et un site plus agréable ; or, j’ai l’impression d’être venu me fourrer dans des bagarres inextricables.*
#### (Lettre 1111, à Jean Louis Vives, Louvain, juin 1520)

 
## LUTHER
*Je ne suis ni l’accusateur de Luther, ni son protecteur, ni son juge. Je n’oserais juger l’esprit de cet homme (…).Il est enfin chrétien, je pense, de favoriser Luther de manière à ne pas permettre, s’il est innocent, que des factions malhonnêtes l’abattent ; s’il est dans l’erreur, je souhaiterais sa guérison et non sa perte, ce qui s’accorde mieux avec l’exemple du Christ…*
*… on a conçu le soupçon que les livres de Luther étaient en grande partie mon œuvre et qu’ils étaient nés à Louvain, quand il n’y a pas un trait de plume qui soit mien, ou qui ait été publié à ma connaissance ou avec mon consentement.*
#### (Lettre 1033, à Albert de brandebourg, cardinal archevêque, 19 octobre 1519) {.right}
*Je ne te blâme pas d’avoir adopté contre nous une attitude malveillante afin de sauvegarder tes intérêts menacés par mes ennemis papistes.(…) Puisque nous voyons que le Seigneur ne t’a pas donné assez de courage et de force pour combattre l’abomination à nos côtés, librement et en toute confiance, nous ne prétendons pas exiger de toi ce qui dépasse tes forces. (…) Si tu ne peux faire autrement, sois un simple spectateur de notre tragédie, mais ne t’allie pas à nos adversaires et ne leur amène pas des troupes. Avant tout, n’écris pas contre nous et nous n’écrirons pas contre toi.*
#### (Lettre de Luther à Erasme, cité par HALKIN, p. 230) {.right}
*Si tu écris contre moi, je ne m’en inquiéterai guère. A considérer mon intérêt personnel, rien ne pourrait m’arriver de meilleur.*
#### (Lettre 1445, du 8 mai 1521 : réponse d’Erasme à Luther qui le menace d’écrire contre lui. Cité par Halkin, p.230) {.right}

 
## MODERATION, vertu cardinale
*Je me suis toujours gardé de rien écrire d’impur, de séditieux, de contraire à la doctrine du Christ. Jamais, en conscience, je ne serai un maître d’erreur ou l’auteur d’un désordre ; je suis disposé à tout souffrir plutôt que de provoquer une rupture.*
#### (Lettre 1033, à Albert de brandebourg, cardinal archevêque, 19 octobre 1519) {.right}
*Un jugement précipité est condamnable dans tous les cas, mais surtout quand il peut amener la perte d’un homme. (…) Je n’ai jamais admis, et n’admettrai jamais, qu’un homme soit ainsi accablé publiquement sous les clameurs, avant même que ses livres n’aient été lus et examinés à fond, avant que l’homme n’ait été averti de son erreur, avant que ses ouvrages n’aient été réfutés par des preuves et par le témoignages des Saintes Ecritures.*
*… car, pour hurler contre quelqu’un, du haut d’une chaire, en l’appelant « bête féroce » et « antéchrist », nul besoin d’un théologien : le premier bouffon venu pourra en faire autant.*
#### (Lettre 1153, à Godescalc Rosemondt, Louvain, 18 octobre 1520) {.right}
*Le roi d‘Angleterre (…) souhaiterait que tu eusses écrit certaines choses avec plus de prudence et de modération. C’est ce que souhaitent aussi, mon cher Luther, les gens qui te veulent du bien. C’est grave de provoquer des hommes qu’on ne peut réduire sans amener beaucoup d‘agitation dans l’état des choses (…) Si on laisse entrer la mer, on n’est plus maître de régler son débit. Si une action réclame de l’agitation, je préférerais qu’un autre que moi fût responsable de l’agitation. Pourtant je ne m’oppose pas à tes vues : je crains, si elles sont inspirées par le Christ, de m’opposer à Lui. Je te demande seulement de ne pas mêler maladroitement à tes écrits mon nom et celui de mes amis…*
#### (Lettre 1127A, à Martin Luther, Louvain, 1er août 1520) {.right}

 
## MOINES, cette engeance
*… cette race superstitieuse qui, en partie pour servir leurs intérêts, en partie en vertu d’un zèle immense mais peu éclairé, parcourent la terre et la mer et, dès qu’ils rencontrent un homme qui renonce aux vices pour se convertir à une vie meilleure, tâchent aussitôt, à grand renfort d’exhortations des plus malhonnêtes, de menaces et de flatteries, de le précipiter dans l’état monacal, comme si le christianisme n’existait pas en dehors du froc. Une fois qu’ils lui ont bourré le cœur de purs scrupules et de ronces inextricables, ils le plient à de petites observances purement humaines (…) en lui enseignant à trembler et non à aimer.*
*L’état monacal n’est pas la piété ; il n’est qu’un genre de vie, bon ou mauvais selon la constitution de corps et d’esprit de chacun.*
#### (Lettre 164, à Jean, un ami vivant à la cour, Saint-Omer, automne 1501) {.right}

 
## MORE Thomas, le plus charmant des amis
*La nature a-t-elle jamais rien produit de plus souple, de plus doux, de plus heureux que le génie de Thomas More ?*
#### (Lettre 118, à Robert Fisher, Londres, 5 décembre 1499) {.right}
*J’ai pour lui une telle affection que s’il m’ordonnait de danser (…), je lui obéirais sans m’appesantir. (…) Si ma vive affection pour lui ne me trompe pas, jamais, je pense, la nature n’a créé une intelligence plus rapide, plus ouverte, plus pénétrante, plus aiguë, bref, plus comblée des dons les plus divers. A quoi s’ajoute une parole égale à l’intelligence, et un caractère d’un enjouement admirable, beaucoup de piquant, mais toujours bienveillant, si bien que rien ne manque en lui de ce qui compose le parfait avocat.*
*… le plus doux des amis, celui avec lequel j’aime à mettre en commun les choses sérieuses et les plaisantes.*
#### (Lettre 191, à Richard Whitford, à la campagne, 1er mai 1506) {.right}

 
## NAISSANCE illégitime
*Ton zèle pour la religion, l’honnêteté de ta vie et de tes mœurs et les louables mérites que te valent ta probité et des vertus dont un témoignage digne de foi te recommande auprès de Nous, Nous incitent à t’accorder des faveurs et des grâces spéciales. C’est pourquoi, étant donné, ainsi que tu l’assures, que tu souffres d’une tache de naissance, étant né d’un célibataire et d’une veuve, voulant, en considération de tes mérites susmentionnés, par une faveur toute gratuite, te relever de toutes excommunications (…) et te permettre de te considérer comme effectivement absous…*
#### (Lettre 187 A, Du pape Jules II à Erasme, Rome, 4 janvier 1505) {.right}

 
## NEUTRALITE : position dangereuse et mal comprise
*Je ne suis du parti d’aucun homme. Je déteste ces noms de dissidence. Je suis chrétien et je ne connais que les chrétiens ; je ne supporterais pas d’Erasmistes…*
#### (Lettre 1041, au lecteur (préface d’un livre), Louvain, novembre 1519) {.right}
*On dit de toi des choses contradictoires. Prends garde, en essayant de retenir les deux factions pour l’amour de toi, de provoquer la haine des uns et des autres.*
#### (Lettre de Capiton, luthérien,  à Erasme ; cité par Halkin, p. 229) {.right}
*J’aime la liberté. Je ne veux ni ne peux être au service d’aucune faction. Je ne peux pas ne pas exécrer la dissension. Je ne peux pas ne pas aimer la paix et la concorde. Que vais-je décider ? Si je ne condamne pas Luther tout entier, je vois bien à quel parti j’apporterai de l’aide et je vois aussi tout le mal que je ferai. Si je l’approuve tout entier, j’agirai imprudemment, car j’approuverais alors ce que je ne comprends pas et je m’engagerais dans une faction dont les adhérents me déplaisent pour la plupart. Si je m’efforce d’être équitable à l’égard des deux partis, acceptant ceci et rejetant cela, je me livre sans défense aux attaques et je n’aurai servi qu’à provoquer de nouveaux désordres. C’est pourquoi il me paraît raisonnable de ne pas me manifester jusqu’au jour où princes et savants, oubliant leurs préjugés, chercheront le moyen d’assurer sans désordre la victoire de la vérité pour la gloire du Christ.*
#### (Eponge, cité par Halkin p.229-230) {.right}

 
## NOUVEAU TESTAMENT, trop sacré pour être traduit ?
*(Standish, franciscain, théologien et évêque anglais) faisait un sermon au cimetière de Saint-Paul à Londres. (…) il commença à se déchaîner contre mon nom et ma réputation, prétendant que la destruction complète de la religion chrétienne imminente si toutes les traductions nouvelles ne sont pas immédiatement retirées de la vente. Il dit que la situation ne pouvait se supporter plus longtemps dès le moment où Erasme avait osé corrompre l’évangile selon saint Jean : alors que l’Eglise avait lu pendant tant d’années « au commencement était le verbe », il introduisait une nouvelle lecture : « Au commencement était la parole ».*
#### (Lettre 1126, d’Erasme à Hermann Busch, Louvain, 31 juillet 1520) {.right}

 
## PEDAGOGIE
*Que ton premier soin soit de te choisir un précepteur des plus instruits, car, faute d’être instruit lui-même, nul ne saurait bien instruire autrui.*
*Dès que tu l’auras trouvé, mets tout en œuvre afin qu’il éprouve pour toi les sentiments d’un père et toi, pour lui, ceux d’un fils. (…) cette amitié est d’une telle importance dans l’apprentissage qu’il est inutile d’avoir un maître de lettres si tu n’as pas en lui un ami. Montre-toi ensuite attentif et assidu à ses leçons. Un effort exagéré étouffe parfois les dons naturels des élèves. L’assiduité, au contraire, est une vertu de juste milieu, qui peut se maintenir.(…) Il faut relâcher de temps en temps la tension d’esprit qui naît de l’étude, y intercaler des jeux, mais des jeux faits pour des hommes bien élevés, dignes des bonnes lettres et restant à leur niveau.*
*… Apprends tout d’abord et tout de suite ce qu’il y a de meilleur. (…) Ce qui compte pour toi au début est plus la qualité que la quantité de tes acquisitions.*
*… Au début, et c’est l’essentiel, écoute attentivement, avidement l’explication de ton précepteur. Confie  toutes ses paroles à ta mémoire et les plus importantes à tes cahiers.(…) Ne commets pas l’erreur d’avoir des cahiers pleins de science alors que la tienne sera nulle. Pour éviter que ne s’envole ce que tu as entendu, répète-le pour toi-même ou avec d’autres. La discussion également est comme une palestre qui révèle les muscles de l’esprit, les stimule et les fortifie. Ne rougis pas de questionner si tu as un doute, d’être corrigé si tu t’es trompé. Evite les veillées prolongées : elles éteignent l’intelligence et nuisent gravement à la santé. L’aurore est l’amie des Muses, faite pour l’étude. Après le déjeuner, va jouer, va faire un tour ou cause gaiement avec des amis. Même là, n’y a-t-il pas moyen d’apprendre ? Mesure ta nourriture d’après ta santé, non d’après ta gourmandise. Promène-toi un peu avant le dîner et aussi après. Lis avant de t’endormir une page particulièrement belle et digne d’être retenue, afin que tu y penses quand le sommeil te gagnera et que tu la retrouves en toi-même en te réveillant. Que ce mot de Pline soit toujours présent à ton esprit, que le temps est perdu que tu n’accordes pas à l’étude.*
#### (Lettre 56, à Christian Northoff, Paris, printemps 1497 ?) {.right}

 
## POSTERITE : insolence visionnaire
*Fais comprendre à Madame, que je lui apporterai une gloire incomparablement plus grande que celle qu’elle devra à ces autres théologiens qu’elle nourrit. Eux font des sermons bons pour le vulgaire : ce que moi j’écris durera toujours. Ces bavards incultes, on les écoute dans l’une ou l’autre église, mais mes livres seront lus par les connaisseurs du latin et du grec, dans tous les peuples de l’univers. Des théologiens ignares de cette sorte-là, il y en a partout en foule, mais un homme comme moi, à peine en trouve-t-on un en plusieurs siècles.*
#### (Lettre 139, à Jacques Batt, en parlant de Madame Ane de Veere, mécène potentiel, Orléans, vers le 12 décembre 1500) {.right}

 
## THEOLOGIE, science barbare
*Que dirais-tu si tu voyais Erasme assis bouche bée au milieu des saints disciples de Scot, tandis que du haut de la chaire Gyllard explique son texte ? Si tu voyais son front plissé, ses yeux fixes, son visage tendu ? Tu dirais que ce n’est pas lui. Ces gens affirment que les mystères de cette discipline sont inaccessibles à ceux qui ont le moindre commerce avec les Muses ou avec les Grâces. Si tu as touché aux belles lettres, il te faut désapprendre ; ce que tu as bu à l’Hélicon, il te faut le revomir. Je fais mon possible pour ne rien dire qui soit latin, rien qui soit plaisant ou agréable ; il me semble que j’y arrive ; il y a espoir pour qu’ils reconnaissent un jour Erasme pour l’un des leurs.*
#### (Lettre 64, à Thomas Grey, Paris, août 1497) {.right}
 
## THEOLOGIENS, autre engeance
*Des hommes, à qui la clémence convenait surtout, ne semblent avoir soif que de sang humain et désirer uniquement avec avidité que Luther soit pris et perdu. C’est agir en bourreau et non en théologien. S’ils veulent se montrer grands théologiens, qu’ils convertissent les Juifs, qu’ils convertissent au Christ ceux qui l’ignorent, qu’ils corrigent les moeurs publiques des chrétiens, car il n’y a rien de plus corrompu, même chez les Turcs.*
#### (Lettre 1033, à Albert de Brandebourg, cardinal archevêque, 19 octobre 1519) {.right}

 
## UNIVERSALISME
*… je ne crois pas que l’endroit où chacun est né présente un quelconque intérêt : je considère que c’est la marque d’un orgueil peu fondé, si une ville ou un pays se vante d’avoir donné le jour à un individu quelconque, qui est devenu éminent ou célèbre, grâce à ses propres travaux, et non avec l’aide de sa patrie. Il est plus juste de glorifier le pays qui lui a permis de devenir un grand homme que celui qui lui a donné naissance. Mais voilà que je parle comme s’il y avait en moi quelque chose dont ma patrie pût se vanter ! (…) Si j’éprouvais un sentiment de ce genre, je souhaiterais que ce ne fusse pas seulement la France et l’Allemagne qui, l’une et l’autre, me revendiquent comme leur, mais que tous les pays et toutes les villes rivalisent pour réclamer Erasme.*
#### (Lettre 1147, A Pierre Manius, Louvain, 1er octobre 1520) {.right}
*Mes écrits n’ont jamais été au service d’un parti, sauf à celui du Christ, qui appartient à tous. Ce que je vaux par mon intelligence, ce que je vaux par mes ouvrages, je l’ignore ; mais ce qui est certain, c’est que je me suis efforcé – et que j’ambitionne encore – d’être utile non seulement aux Allemands, mais encore aux Français, aux Espagnols, aux Anglais, aux Tchèques, aux Russes, et même aux Turcs et aux Sarrasins si je puis.*
#### (Lettre 1202, à Josse Jonas, Louvain, 10 mai 1521) {.right}
*Ces agitations des Rois n’annulent pas le pacte des Muses.*
#### (Lettre 1233, à Guillaume Budé, à propos d’une rencontre entre Charles Quint et des représentants anglais et français, accompagnés de nombreux humanistes, Anderlecht, 1521) {.right}

 
## VATICAN
*Plût au ciel que fussent fausses les accusations de Luther contre la curie romaine, tyrannie, avarice, mœurs dissolues !*
#### (Lettre à Pierre Barbier, citée par HALKIN, p.113) {.right}
*Quant aux souverains pontifes qui sont les vicaires du Christ, s’ils s’efforçaient d’imiter sa vie, c’est-à-dire sa pauvreté (…) qu’y aurait-il sur terre de plus malheureux ? (…) Que d’avantages leur enlèverait la sagesse, si elle s’emparait d’eux une seule fois ! Que dis-je la sagesse, mais un seul grain de ce sel dont a parlé le Christ ! Tant de richesses, tant d’honneurs, tant d’autorité, tant de victoires, tous ces offices, toutes ces dispenses, tous ces impôts, toutes ces indulgences, tant de chevaux, de mulets, de gardes, de plaisirs. (…) tous ces rédacteurs, tous ces copistes, tous ces notaires, tous ces avocats, tous ces promoteurs, tous ces banquiers, tous ces entremetteurs (pour peu j’ajouterais un mot plus tendre, mais je crains d’offenser vos oreilles), en somme cette immense foule si onéreuse, pardon, je voulais dire si honorable, serait réduite à la famine. C’est vrai, ce serait un acte  inhumain et abominable, mais il serait encore beaucoup plus détestable que les grands princes de l’Eglise eux-mêmes, vraies lumières du monde, soient ramenés à la besace et au bâton.*
*En réalité, ce qu’il y a de pénible, ils le laissent à Pierre et Paul, qui ont bien du loisir de reste. Mais tout ce qu’il y a de lustre et de volupté, ils se le réservent. (…) Faire des miracles est suranné, désuet, ce n’est plus du tout de notre époque ; instruire le peuple est fatigant ; expliquer les Saintes Ecritures, c’est l’affaire de l’école ; prier, c’est oiseux (…) ; être pauvre, sordide (…) enfin, mourir est déplaisant ; être mis en croix, infamant.*
#### (Eloge de la Folie, LIX, éd. J.-C. Margolin, p. 130-131) {.right}

 
## VIE HUMAINE (respect de la)
*Je vois des luthériens, je ne vois que peu ou point d’évangéliques. (…) Hutten (partisan de Luther) prétend qu’il faut affronter la mort pour la liberté évangélique. Moi non plus, je ne m’y déroberais pas, si quelques nécessités m’y contraignaient, mais je ne suis pas encore disposé à affronter la mort pour Luther et pour les paradoxes de Luther. En effet, il ne s’agit pas des articles de la foi, mais de savoir si la primauté du pontife romain vient du Christ, si le collège des cardinaux est un membre indispensable de l’Eglise, si la confession a pour auteur le Christ (…). Pour ces questions, qui sont thèmes ordinaires de débats scolastiques, je n’oserais arracher la vie à un homme, si j’étais juge, ni risquer ma propre vie. Je souhaiterais devenir martyr pour le Christ, s’il m’en donne la force ; je ne voudrais pas être martyr pour Luther.*
#### (Eponge. Cité par Halkin, p.256-257) {.right}

