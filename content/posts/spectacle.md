---
title: "Spectacle"
date: 2021-07-25T15:00:54+02:00
draft: true
---
A travers livres, lettres et pages chiffonnées, suivez le parcours d’Erasme, moinillon rebelle qui veut vivre de sa plume. Apprenez avec lui comment démasquer la Folie du monde, dire leurs quatre vérités aux plus puissants tout en échappant à l’Inquisition (ça peut toujours servir). Une plongée pleine d’encre et de papier dans le redoutable 16e siècle.

*We volgen Erasmus’ levenspad aan de hand van boeken, brieven en beduimelde bladzijden. Het rebels monnikje Erasmus wenst van zijn pen te leven. Ontdek samen met hem hoe de Waanzin van de wereld kan worden ontmaskerd. Hoe men ongezouten de waarheid aan de machti- gen der aarde kan verkondigen en toch aan de inquisitie kan ontsnappen (dit heeft zo zijn voordelen). Een sprong in de schrikwekkende zestiende eeuw, met inkt en papier als enig wapen.*

[Sources](/posts/sources) | [Photos](/photos) | [Extrait vidéo](https://www.youtube.com/watch?v=n3dGH0Y4L4o&feature=youtu.be) | [Presse](/posts/presse) | [Fiche technique](/fiche-erasme.pdf)

**Théâtre d’objets.**

* **Durée : env. 60 min.**
* **Tout public à partir de 12 ans**
* **Séance scolaire à partir de 15 ans.**

*Objectentheater*

* *Duurt : ong. 60 min.*
* *Vanaf 12 jaar oud.*
* *Voor leerlingen vanaf het 4de middelbaar.*

---

* Narration – *Vertelster* : Ludwine Deblon
* Production – *Productie* : De Capes et de Mots ASBL
* Mise en scène – *Enscenering* : Stéphane Georis
* Transposition en néerlandais et travail scénique – *Hertaler en spelcoach* – : Lucas Tavernier
* Développement des personnages – *Ontwikkeling van de figuren* : Michel Verbeek
* Bande son – *Geluid* : Nicolas Vandooren avec les voix de Stéphane Georis et Nicolas Vandooren en français *en de stem van Lucas Tavernier in het Nederlands*
* Photos – *Fotos* : Christelle Fauville (studio) & Marie-Catherine Gilles (Maison d’Erasme, septembre 2014)

Avec le soutien de – *Met de steun van* :

* La Maison d’Erasme – *Het Erasmus Huis*
* Quilombo
* Sonic Music
