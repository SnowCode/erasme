---
title: "Parents, Tuteurs"
date: 2021-07-25T20:11:40+02:00
draft: true
---
Erasme est le deuxième et dernier enfant du couple, après son **frère** Pierre. De sa **mère**, Maguerite, on sait qu’elle était veuve d’une première union. Autour de son **père** règne, dans les écrits d’Erasme, un flou savamment entretenu. Les biographes actuels retiennent l’hypothèse déjà ancienne d’un père prêtre, copiste de son état.

Malgré les circonlocutions et justifications tracassières de leur fils, ou peut-être à travers elles, se dessine ainsi le portrait d’un **couple non marié**, mais assumé. Les cas de prêtres concubins étaient nombreux à l’époque – les temps ont-ils changé ? –, au point que l’Eglise, jamais en retard d’idées, percevait sans honte une taxe sur leur infraction (Cfr HALKIN p.189).
 
Erasme perdit sa mère lorsqu’il avait environ 13 ans, puis son père trois ans plus tard, à chaque fois lors d’une **épidémie de peste**, endémique en Europe. Toute sa vie, Erasme fuira avec terreur la peste, quittant séance tenante les villes contaminées. Faut-il s’en étonner ?
 
Orphelins, les deux frères furent confiés à des **tuteurs** qu’Erasme accusera, bien après, d’avoir dilapidé leur héritage et forcé leur vocation monastique. On perd trace de Pierre, dont son puîné ne semble pas regretter la compagnie.

Je n’ai pas trouvé non plus, dans les écrits d’Erasme, de grands épanchements sentimentaux pour ses parents. Toutefois, sa naissance illégitime explique assez sa discrétion sur le sujet : la **bâtardise** était un obstacle sérieux à de nombreux honneurs et carrières. Dispense après dispense, Erasme les contournera tous.
 
Enfin, il me semble qu’un tel caractère, si bien trempé, a dû recevoir très tôt de ses parents l’affectueuse assurance de sa valeur et de son droit à être aimé. C’est cette image que j’ai gardée, brève mais déterminante, au début de ma narration.
