---
title: "Presse"
date: 2021-07-25T18:49:32+02:00
draft: true
---
![/presse1.jpg](/presse1.jpg)

Amélie Dogot, Erasme, d’encre et de papier chiffonné, in *Espace de Liberté* (Centre d’Action Laïque) n°449, mai 2016, p.76-77.

![presse2](/presse2.jpg)

Nathalie Vincent, Erasme, Une boulette de papier en quête de liberté, in *[Anderlecht Contact](http://www.anderlecht.be/images/stories/AC/ac147.pdf)*, Juin 2015, p.15.

![presse3](/presse3.jpg)

Nathalie Vincent, Erasmus : papierprop op zoek naar vrijheid, *[Anderlecht Contact](http://www.anderlecht.be/images/stories/AC/ac147.pdf)*, juni 2015, p.15.

Anamaria Olaru, [The Erasmus House, A Historical & Cultural Complex Not To Be Missed](http://theculturetrip.com/europe/belgium/articles/the-erasmus-house-a-historical-cultural-complex-not-to-be-missed/), in *The CultureTrip.com*, 14 February 2016

