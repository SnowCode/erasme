---
title: "Quel Caractere"
date: 2021-07-25T20:18:48+02:00
draft: true
---
*… un drôle de caractère, mais, derrière l’ironie, le dédain, l’apparente froideur, un grand désarroi, une immense détresse ; un caractère hautain, un orgueil accusé, une égoïste et sénile indifférence mais aussi, chez cet homme bourru, en somme, un bon cœur ; une méfiance excessive, une rancune tenace, mais une bienveillance souvent désarmante. Dans ses dernières années, il est lui-même, il ne parade plus, il est l’acteur de son propre drame, celui de la vieillesse ; il assiste à la mort de ses amis les plus chers – et il en est plus affecté qu’il ne paraît – il voit la ruine de ses efforts, le triomphe de cette violence, dont l’amoureux du juste milieu qu’il est a toujours été l’ennemi et il se retrouve seul, devant lui-même et en face de la mort. Il redevient lui-même, disions-nous. (…) ce personnage aux mille facettes tour à tour orgueilleux et humble, méfiant et confiant, pingre et magnanime, audacieux et craintif, c’est Erasme, au même titre que le vieillard amer et déçu qui meurt à Bâle, au même titre que le jeune secrétaire particulier avide de découvrir le monde, que l’homme d’affaires chicaneur, l’excellent cavalier, le brillant convive, le promeneur amoureux des jardins, l’autoritaire maître de maison qui malmène ses subordonnés mais craint Margaret. Oui, cette galerie des portraits, cette Comédie Humaine, c’est Erasme, protée aux cent visages.*

#### (BIERLAIRE, p.112. Cfr Sources) {.right}
