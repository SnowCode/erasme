---
title: "Expertise linguistique"
date: 2021-07-25T20:35:49+02:00
draft: true
---
Pour Erasme, le **latin** est tout à la fois un outil d’expression commode, un passeport international, une élégante carte de visite et un moyen de gagner sa vie. Il consacre de nombreux ouvrages à l’étude de cette langue.

Comme professeur de latin, il rédige des traités et ouvrages d’apprentissage pour ses **élèves**, comme :

* *Le Traité d’art épistolaire*
* *Le Traité de la double abondance des mots et des choses*
* *Les Colloques*
* *Les Adages*

Souvenons-nous que de son temps, il n’existe **aucun dictionnaire**, aucune grammaire complète de latin. Ces outils sont précisément inventés à son époque.

 
Erasme, infatigable correspondant, constate que ses lettres sont détournées, copiées et publiées à son insu, à titre de modèles. Il entreprend dès lors lui-même la **compilation** de sa correspondance. Il en profite pour s’y présenter sous le jour qui lui convient le mieux.

 
La défense des « bonnes lettres » lui suggère d’écrire tantôt contre les « barbares » qui les méconnaissent  (*Les Antibarbares*), tantôt contre les intégristes du latin antique, qui refusent son usage contemporain (*Le Cicéronien*).

 
Au cours de son travail de philologue, il produit des **commentaires**, comme les *Annotations sur le Nouveau Testament de Valla*.

 
Tous ces livres combinent style élégant et réflexions philosophiques et morales.

