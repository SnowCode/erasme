---
title: "Les Guerres De Religion"
date: 2021-07-25T20:50:14+02:00
draft: true
---
Erasme naît dans une Europe occidentale **unie par une même religion**, le christianisme. Pape, cardinaux, évêques, prêtres, religieux et moines contrôlent les âmes à tous les étages de la société. La piété est intense – et obligatoire. Erasme, lui-même religieux et prêtre, proteste  contre les **abus** de ce carcan : vocations forcées, corruption du clergé, contraintes innombrables, superstitions dégradantes, dogmes abscons et cette affreuse police de la pensée qu’on appelle l’**Inquisition**. Il prône le retour au bon sens, à la modération, à une certaine liberté de pensée individuelle et un rapport à Dieu empreint de **douceur**.
 
### Les papes, le Vatican 
Erasme a connu de nombreux papes au cours de sa vie. Lors de son voyage en Italie, il a mesuré l’étendue de la **corruption* vaticane.
 
Parmi tous les excès des papes de cette époque figure en bonne place la vente des **indulgences** – plus vous payez maintenant, plus vite vous entrerez au paradis à votre mort.
 
Cette contestable pratique s’intensifie au début du 16e siècle pour contribuer à la construction de la nouvelle **basilique** du Vatican.
 
### Martin Luther 
Moine allemand, érudit, angoissé par l’idée du salut de son âme, Martin Luther **s’oppose** avec fracas à la vente des indulgences, puis, progressivement, à tous les abus de l’Eglise catholique.
 
Il partage de nombreuses idées d’Erasme, mais sa **véhémence**, son emportement rebutent ce dernier, épris de modération.
 
Luther sollicite l’adhésion d’Erasme en 1519. Erasme louvoie. Luther s’énerve, menace. Erasme, qui compte des amis parmi les luthériens comme parmi les papistes, refuse de s’engager dans le conflit.
 
Pressé de toutes parts, Erasme s’explique finalement, dans son traité du « ***Libre Arbitre*** », sur ce qui l’oppose à Luther. Pour l’Allemand, en effet, l’Homme est mauvais, ses actes sont tous péchés et seule la grâce de Dieu peut le sauver. Pour Erasme, au contraire, l’Homme est capable de bonnes choses et le **paradis** s’ouvre à toute créature, même païenne, recherchant sincèrement Dieu.
 
Autour d’Erasme, la rupture sera complète : Luther, radical, le honnira pour sa tiédeur. De son côté, l’Eglise catholique, irritée de ses critiques, le mettra à l’index après sa mort, comme « **hérétique** de première classe » . La Maison d’Erasme, à Anderlecht, donne à voir de nombreux ouvrages d’Erasme minutieusement censurés après sa mort.
 
Il est des époques vouées aux intégrismes, où il ne fait pas bon prêcher le **dialogue** et la modération. L’Europe que quitte Erasme au soir de sa vie est **déchirée** entre catholiques et protestants. Ainsi s’installent, pour de longs siècles, les horreurs des guerres de religion.
