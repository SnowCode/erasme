---
title: "Des Rois Remuants"
date: 2021-07-25T20:47:39+02:00
draft: true
---
Parmi les nombreux rois d’Europe qu’Erasme a connus au cours de sa longue vie, j’en ai retenu trois, très emblématiques.
 
### Charles Quint 
Charles Quint, roi d’Espagne, duc de Bourgogne, empereur germanique et souverain d’un continent entier, l’Amérique. Par sa naissance aux Pays-Bas, Erasme est son sujet. Il sera aussi son **conseiller officiel**, tâche dont il esquive les fastidieuses mondanités et ne retient que des promesses, souvent vaines, de pension. Dans la tourmente de la Réforme, Charles Quint restera imperturbablement **catholique**.

### François Ier
François 1er, roi de France, est l’ennemi juré de Charles Quint, dont l’empire le cerne de toutes parts. Les guerres entre eux sont incessantes. François cherche à attirer Erasme à Paris, pour y créer le **Collège de France**, sur le modèle du Collège des Trois langues de Louvain. Mais l’humaniste se méfie de Paris : là aussi, les bûchers de l’Inquisition se sont rallumés. Erasme, sans refuser clairement, esquive l’invitation.

### Henri VIII 
Henri VIII, roi d’Angleterre, n’est qu’un gamin lorsqu’Erasme le rencontre pour la première fois. Elégant, érudit, généreux, le jeune roi soulève d’immenses espoirs lorsqu’il monte sur le trône. Dans le conflit luthérien, sa place est ambiguë : ce qui l’intéresse avant tout, c’est de briser la sujétion de l’Angleterre au pape, ses impôts et ses diktats. Erasme entretient une correspondance suivie avec lui. Henri VIII passera à la postérité comme le fondateur de l’Eglise anglicane et le **Barbe Bleue** qui a épousé – épuisé – six reines à lui tout seul.

 
